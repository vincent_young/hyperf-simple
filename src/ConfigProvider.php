<?php

namespace lgdz\hyperf;

use lgdz\exception\handler\AppExceptionHandler;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            // 合并到  config/autoload/dependencies.php 文件
            'dependencies' => [],
            // 与 commands 类似
            'listeners'    => [],
            // 合并到  config/autoload/annotations.php 文件
            'annotations'  => [
                'scan' => [
                    'paths' => [
                        __DIR__,
                    ],
                ],
            ],
            // 亦可继续定义其它配置，最终都会合并到与 ConfigInterface 对应的配置储存器中
        ];
    }
}